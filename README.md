


https://blog.palo-it.com/en/spring-boot-client-and-server-code-generation-using-openapi-3-specs

https://blog.mimacom.com/using-the-openapi-generator-for-spring-boot/


http://localhost:8080/swagger-ui oppure http://localhost:8080


mvn clean compile

### Generate server stub
cd poc-openapi-generator
./mvnw clean generate-sources -P openapi-server

### Generate client
cd poc-openapi-generator
./mvnw clean generate-sources -P openapi-client

### Run application
cd poc-openapi-generator
./mvnw spring-boot:run

### Tests
cd poc-openapi-generator
./mvnw clean verify
